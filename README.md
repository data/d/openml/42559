# OpenML dataset: medical_charges_nominal

https://www.openml.org/d/42559

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Centers for Medicare & Medicaid Services  
**Source**: [original](https://www.cms.gov/Research-Statistics-Data-and-Systems/Statistics-Trends-and-Reports/Medicare-Provider-Charge-Data/Downloads/Inpatient_Data_2011_CSV.zip) - 14-08-2018  
**Please cite**: Patricio Cerda, Gael Varoquaux, Balazs Kegl. Similarity encoding for learning with dirty categorical variables. 2018. Machine Learning journal, Springer.  

The Inpatient Utilization and Payment Public Use File &#40;Inpatient PUF&#41; provides information on inpatient discharges for Medicare fee-for-service beneficiaries. The Inpatient PUF includes information on utilization, payment (total payment and Medicare payment), and hospital-specificcharges for the more than 3,000 U.S. hospitals that receive Medicare Inpatient Prospective Payment System &#40;IPPS&#41; payments. The PUF is organized by hospital and Medicare Severity Diagnosis Related Group (MS-DRG) and covers Fiscal Year (FY) 2011 through FY 2016.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42559) of an [OpenML dataset](https://www.openml.org/d/42559). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42559/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42559/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42559/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

